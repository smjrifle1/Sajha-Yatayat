<?php include('includes/header.php');?>
<?php include('includes/navigation.php');?>
<?php include('controller/functions.php');?>

<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>Buses</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc placerat risus ac augue pulvinar pellentesque. Donec vitae lacinia mi. Cras vel pretium mi, nec egestas dui. Fusce euismod ligula nec erat semper posuere. Curabitur ac ipsum elit. Interdum et malesuada fames ac ante ipsum primis in faucibus. Curabitur viverra vehicula felis. Phasellus blandit gravida nisi quis rutrum. Cras id efficitur erat, in vehicula nulla. In dignissim justo ac justo pharetra gravida.</p><br><br>
        <div class="row">

          <?php 
          $qur = "SELECT * FROM yatayat t
          WHERE NOT EXISTS (SELECT 1
            FROM yatayat t2
            WHERE t2.deviceID = t.deviceID and t2.timesStamp > t.timesStamp
            )";
            $query = mysql_query($qur);
            if(mysql_num_rows($query) >= 1){
              while ($row = mysql_fetch_array($query)) : ?>
              <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="bus-item">
                  <div class="bus-img" style="background-image: url('images/bus-img1.jpg');">
                    <a href="bus-detail.php<?php echo '?busNo='.$row['deviceID']. '&busRoute='.$row['route'].'&busLoc='.extractReverseGeocoding($row['lat'],$row['lon']) ;?>">
                      <div class="bus-info"><span>Current Location: <br><?php echo extractReverseGeocoding($row['lat'],$row['lon']); ?> </span></div>
                      <div class="bus-num"><?php echo $row['deviceID']; ?></div>
                    </a>
                  </div>
                </div>
              </div> 
            <?php endwhile;?>
            <?php } ?>

            </div>
          </div>
        </div>  
      </div>
    </section>

<?php include('includes/footer.php');?>    