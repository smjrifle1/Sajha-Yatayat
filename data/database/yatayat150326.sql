-- MySQL dump 10.13  Distrib 5.5.31, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: sajha
-- ------------------------------------------------------
-- Server version	5.5.31-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `yatayat`
--

DROP TABLE IF EXISTS `yatayat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yatayat` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `status` varchar(100) NOT NULL,
  `route` varchar(100) NOT NULL,
  `lat` varchar(100) NOT NULL,
  `lon` varchar(100) NOT NULL,
  `speed` varchar(100) NOT NULL,
  `timesStamp` datetime NOT NULL,
  `deviceID` varchar(100) NOT NULL,
  `androidID` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yatayat`
--

LOCK TABLES `yatayat` WRITE;
/*!40000 ALTER TABLE `yatayat` DISABLE KEYS */;
INSERT INTO `yatayat` VALUES (1,'7','testing','85.0','26.0','0.0','2015-03-26 01:30:30','test','null'),(2,'8','testing','85.01231166666666','26.000098333333334','0.0','2015-03-26 01:33:30','test','null'),(3,'8','testing','27.659356666666667','85.32316333333334','0.0','2015-03-26 07:01:50','ba6pa1469','null'),(4,'8','testing','27.70552','85.32226333333334','0.0','2015-03-26 07:21:59','ba6pa1469','null'),(5,'8','testing','27.70975','85.31709166666666','0.0','2015-03-26 07:22:59','ba6pa1469','null'),(6,'3','','1','2','','2015-03-26 23:35:26','3','null'),(7,'3','','1','2','','2015-03-26 23:37:26','3','null');
/*!40000 ALTER TABLE `yatayat` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-03-26 23:58:01
