<?php 
      

      function extractReverseGeocoding($lat,$lon){

            $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.$lat.','.$lon;
            $data = file_get_contents($url);
            // parse the json response
            $jsondata = json_decode($data,true);
            // if we get a placemark array and the status was good, get the addres
            if(is_array($jsondata))
            {
              return $jsondata ['results'][0]['address_components'][0]['long_name'];
            }


      }

?>